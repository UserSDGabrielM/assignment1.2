<%--
  Created by IntelliJ IDEA.
  User: MANOWAR
  Date: 11/8/2016
  Time: 2:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Success</title>
</head>
<body>
<table border=1>
    <thead>
    <tr>
        <th>Flight number</th>
        <th>Airplane type</th>
        <th>Departure city</th>
        <th>Arrival city</th>
        <%--<th colspan=2>Action</th>--%>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${flights}" var="flight">
        <tr>
            <td><c:out value="${flight.flightNumber}" /></td>
            <td><c:out value="${flight.airplaneType}" /></td>
            <td><c:out value="${flight.departureCity}" /></td>
            <td><c:out value="${flight.arrivalCity}" /></td>
            <%--<td><a href="UserController?action=edit&userId=<c:out value="${user.userid}"/>">Update</a></td>--%>
            <%--<td><a href="UserController?action=delete&userId=<c:out value="${user.userid}"/>">Delete</a></td>--%>
        </tr>
    </c:forEach>
    </tbody>
</table>

<a href="/">Logout</a>

</body>
</html>
