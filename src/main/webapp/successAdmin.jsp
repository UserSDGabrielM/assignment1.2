<%--
  Created by IntelliJ IDEA.
  User: MANOWAR
  Date: 11/8/2016
  Time: 2:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Success</title>
</head>
<body>
<table border=1>
    <thead>
    <tr>
        <th>Flight number</th>
        <th>Airplane type</th>
        <th>Departure city</th>
        <th>Arrival city</th>
        <th colspan=2>Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${flights}" var="flight">
        <tr>
            <form method="post">
                <td><input name="flightNumber" value="${flight.flightNumber}"/></td>
                <td><input name="airplaneType" value="${flight.airplaneType}"/></td>
                <td><input name="departureCity" value="${flight.departureCity}"/></td>
                <td><input name="arrivalCity" value="${flight.arrivalCity}"/></td>
                    <%--<td><a href="UserController?action=edit&userId=<c:out value="${flight}"/>">Update</a></td>--%>
                    <%--<td><a href="UserController?action=delete&userId=<c:out value="${flight}"/>">Delete</a></td>--%>
                <td>
                    <form action="updateFlight" method="post">
                        <input type="hidden" name="flightId" value="${flight.id}"/>
                        <input type="hidden" name="operation" value="update">
                        <input type="submit" value="Update"/>
                    </form>
                </td>
                <td>
                    <form action="deleteFlight" method="post">
                        <input type="hidden" name="flightId" value="${flight.id}"/>
                        <input type="hidden" name="operation" value="update">
                        <input type="submit" name="delete" value="Delete"/>
                    </form>
                </td>
            </form>
        </tr>
    </c:forEach>
    <tr>
        <form action="addFlight" method="post">
            <td><input name="flightNumber"/></td>
            <td><input name="airplaneType"/></td>
            <td><input name="departureCity"/></td>
            <td><input name="arrivalCity"/></td>
            <td><input type="hidden" name="operation" value="create"></td>
            <td><input type="submit" name="delete" value="Create"/></td>
        </form>

    </tr>
    </tbody>
</table>

<a href="/">Logout</a>

</body>
</html>
