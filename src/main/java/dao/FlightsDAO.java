package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pojo.Flight;

import java.util.List;

public class FlightsDAO {
    List<Flight> flights = null;

    public List<Flight> getAllFlights(){

        try {
            // 1. configuring hibernate
            Configuration configuration = new Configuration().configure();

            // 2. create sessionfactory
            SessionFactory sessionFactory = configuration.buildSessionFactory();

            // 3. Get Session object
            Session session = sessionFactory.openSession();

            // 4. Starting Transaction
            Transaction transaction = session.beginTransaction();
            flights = session.createCriteria(Flight.class).list();
            transaction.commit();
            System.out.println("\n\n Details Added \n");
            session.close();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            System.out.println("error");
        }

        return flights;
    }

    public Flight getFlightById(Integer id){
        Flight flight = null;
        try{
            // 1. configuring hibernate
            Configuration configuration = new Configuration().configure();

            // 2. create sessionfactory
            SessionFactory sessionFactory = configuration.buildSessionFactory();

            // 3. Get Session object
            Session session = sessionFactory.openSession();

            // 4. Starting Transaction
            Transaction transaction = session.beginTransaction();
            flights = session.createCriteria(Flight.class).list();
            transaction.commit();
            session.close();

            for(Flight tmpFlight: flights){
                if(id.equals(tmpFlight.getId())){
                    flight = tmpFlight;
                }
            }
        } catch(HibernateException e){
            System.out.println(e.getMessage());
            System.out.println("error");
        }

        return flight;
    }

    public void updateFlight(Flight flight){
        // 1. configuring hibernate
        Configuration configuration = new Configuration().configure();

        // 2. create sessionfactory
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // 3. Get Session object
        Session session = sessionFactory.openSession();

        // 4. Starting Transaction
        Transaction transaction = session.beginTransaction();
        Object mergedFlight = session.merge(flight);
        session.saveOrUpdate(mergedFlight);
        transaction.commit();
        session.close();
    }

    public void deleteFlight(Flight flight){
        // 1. configuring hibernate
        Configuration configuration = new Configuration().configure();

        // 2. create sessionfactory
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // 3. Get Session object
        Session session = sessionFactory.openSession();

        // 4. Starting Transaction
        Transaction transaction = session.beginTransaction();
        session.delete(flight);
        transaction.commit();
        session.close();
    }

    public void createFlight(Flight flight){
        // 1. configuring hibernate
        Configuration configuration = new Configuration().configure();

        // 2. create sessionfactory
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // 3. Get Session object
        Session session = sessionFactory.openSession();

        // 4. Starting Transaction
        Transaction transaction = session.beginTransaction();

        session.save(flight);
        transaction.commit();
        session.close();

    }
}
