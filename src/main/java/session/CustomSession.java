package session;



public class CustomSession {

    private String username;
    static CustomSession session = new CustomSession();

    private CustomSession(){
    }

    public static CustomSession getInstance(){
        return session;
    }

    public static CustomSession getSession() {
        return session;
    }

    public static void setSession(CustomSession session) {
        CustomSession.session = session;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
