package customservlet;

import dao.FlightsDAO;
import pojo.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class FlightAdminServlet extends HttpServlet{

    List<Flight> flights;
    HttpSession session;

    private int i =0;

    protected void doGet(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        FlightsDAO flightsDAO = new FlightsDAO();
        flights = flightsDAO.getAllFlights();

        response.setContentType("text/html");
        RequestDispatcher view = request.getRequestDispatcher("/successAdmin.jsp");
        request.setAttribute("flights", flights);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        FlightsDAO flightsDAO = new FlightsDAO();
        Flight flight;

        String action = request.getParameter("operation");

        if(action.contains("update")){
            //Update
            Integer flightId = Integer.parseInt(request.getParameter("flightId"));
            flight = flightsDAO.getFlightById(flightId);
            flight.setFlightNumber(request.getParameter("flightNumber"));
            flight.setAirplaneType(request.getParameter("airplaneType"));
            flight.setDepartureCity(request.getParameter("departureCity"));
            flight.setArrivalCity(request.getParameter("arrivalCity"));
            flightsDAO.updateFlight(flight);
        } else if(action.contains("delete")){
            //Delete
            Integer flightId = Integer.parseInt(request.getParameter("flightId"));
            flight = flightsDAO.getFlightById(flightId);
            flightsDAO.deleteFlight(flight);
        } else if(action.contains("create")){
            //Create
            flight = new Flight();
            flight.setId(i);
            i++;
            flight.setFlightNumber(request.getParameter("flightNumber"));
            flight.setAirplaneType(request.getParameter("airplaneType"));
            flight.setDepartureCity(request.getParameter("departureCity"));
            flight.setArrivalCity(request.getParameter("arrivalCity"));
            flightsDAO.createFlight(flight);
        }

        response.sendRedirect("FlightsAdmin");
    }
}
