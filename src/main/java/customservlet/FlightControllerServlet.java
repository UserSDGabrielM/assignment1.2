package customservlet;

import dao.FlightsDAO;
import pojo.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FlightControllerServlet extends HttpServlet {

    List<Flight> flights;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        FlightsDAO flightsDAO = new FlightsDAO();
        flights = flightsDAO.getAllFlights();

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

        RequestDispatcher view = request.getRequestDispatcher("/success.jsp");
        request.setAttribute("flights", flights);
        view.forward(request, response);
    }
}
