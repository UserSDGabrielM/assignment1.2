package customservlet;

import dao.UserDAO;
import pojo.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserLoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException{

        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUserByUserName(userName);
        if(user.getType().equals("ADMIN")){
            response.sendRedirect("FlightsAdmin");
        } else if(user.getType().equals("CLIENT")){
            response.sendRedirect("Flights");
        }
    }

}
